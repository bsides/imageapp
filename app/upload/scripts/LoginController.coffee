fb = new Firebase 'https://vorttex.firebaseio.com/'

angular.module('upload').controller 'LoginController', ($scope, $firebaseAuth, supersonic) ->
  fbAuth = $firebaseAuth(fb)

  $scope.login = (username, password) ->
    fbAuth.$authWithPassword(
      email: username
      password: password).then((authData) ->
      # $state.go 'secure'
      supersonic.ui.layers.push(new supersonic.ui.View 'upload#upload')
      return
    ).catch (error) ->
      console.error 'ERROR: ' + error
      if error.code == 'TRANSPORT_UNAVAILABLE'
        fbAuth.$authWithPassword(
          email: username
          password: password).then((authData) ->
          # $state.go 'secure'
          supersonic.ui.layers.push(new supersonic.ui.View 'upload#upload')
          return
        ).catch (error) ->
          console.error 'ERROR: ' + error
          return
      return
    return

  $scope.register = (username, password) ->
    fbAuth.$createUser(
      email: username
      password: password).then((userData) ->
      fbAuth.$authWithPassword
        email: username
        password: password
    ).then((authData) ->
      # $state.go 'secure'
      supersonic.ui.layers.push(new supersonic.ui.View 'upload#upload')
      return
    ).catch (error) ->
      console.error 'ERROR: ' + error
      return
    return

  return
