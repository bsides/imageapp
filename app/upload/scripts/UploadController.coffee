fb = new Firebase 'https://vorttex.firebaseio.com/'

angular.module('upload').controller 'UploadController', ($scope, $firebaseArray, $cordovaCamera) ->
  $scope.images = []
  fbAuth = fb.getAuth()
  if fbAuth
    userReference = fb.child('users/' + fbAuth.uid)
    syncArray = $firebaseArray(userReference.child('images'))
    $scope.images = syncArray
  else
    $state.go 'firebase'

  $scope.upload = ->
    options =
      quality: 75
      destinationType: Camera.DestinationType.DATA_URL
      sourceType: Camera.PictureSourceType.CAMERA
      allowEdit: true
      encodingType: Camera.EncodingType.JPEG
      popoverOptions: CameraPopoverOptions
      targetWidth: 500
      targetHeight: 500
      saveToPhotoAlbum: false
    $cordovaCamera.getPicture(options).then ((imageData) ->
      syncArray.$add(image: imageData).then ->
        alert 'Image has been uploaded'
        return
      return
    ), (error) ->
      console.error error
      return
    return

  return
